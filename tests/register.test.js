const fs = require('fs');
const path = require('path');
const Database = require('better-sqlite3');
const { ORM, KohanaJS } = require('kohanajs');
const { ORMAdapterSQLite } = require('@kohanajs/mod-database');

ORM.defaultAdapter = ORMAdapterSQLite;

KohanaJS.init({ EXE_PATH: `${__dirname}/registerTest/test`, APP_PATH: `${__dirname}/registerTest/test` });
KohanaJS.initConfig(new Map([
  ['cookie', ''],
  ['session', ''],
  ['auth', ''],
  ['signup', ''],
  ['edm', ''],
]));

require('@kohanajs/mod-crypto');
require('@kohanajs/mod-session');
KohanaJS.classPath.set('helper/Registrar.js', require('../classes/helper/Registrar'));
KohanaJS.classPath.set('model/PasswordIdentifier.js', require('../classes/model/PasswordIdentifier'));
KohanaJS.classPath.set('model/Person.js', require('../classes/model/Person'));
KohanaJS.classPath.set('model/Role.js', require('../classes/model/Role'));
KohanaJS.classPath.set('model/User.js', require('../classes/model/User'));
KohanaJS.classPath.set('model/Login.js', require('../classes/model/Login'));
KohanaJS.classPath.set('Auth.js', require('../classes/Auth'));

const Auth = KohanaJS.require('Auth');

const ControllerAuth = require('../classes/controller/Auth');

describe('register test', () => {
  // copy db
  const target = path.normalize(`${__dirname}/registerTest/db/admin.sqlite`);
  if (fs.existsSync(target))fs.unlinkSync(target);
  fs.copyFileSync(path.normalize(`${__dirname}/registerTest/defaultDB/admin.sqlite`), target);
  const db = new Database(target);

  const target2 = path.normalize(`${__dirname}/registerTest/db/session.sqlite`);
  if (fs.existsSync(target2))fs.unlinkSync(target2);
  fs.copyFileSync(path.normalize(`${__dirname}/registerTest/defaultDB/session.sqlite`), target2);

  afterEach(() => {
    db.exec('DELETE FROM persons');
    db.exec('DELETE FROM users');
    db.exec('DELETE FROM password_identifiers');
  });

  test('constructor', async () => {
    const c = new ControllerAuth({ headers: {}, body: 'name=test&email=test@example.com&username=hello&password=Hello1234!', cookies: {} });
    const r = await c.execute();
    if (r.status === 500)console.log(c.error);
    expect(r.status).toBe(200);
    expect(c.error).toBe(null);
  });

  test('register new user', async () => {
//    KohanaJS.config.auth.databasePath = path.normalize(`${__dirname}/registerTest/db`)
    const c = new ControllerAuth({ headers: {}, body: 'name=test&email=test@example.com&username=hello&password=Hello1234!', cookies: {} });
    const r = await c.execute('signup_post');
    if (r.status === 500)console.log(c.error, c.body);

    const vp1 = db.prepare('SELECT * FROM persons').get();
    expect(vp1.first_name).toBe('test');

    const v1 = db.prepare('SELECT * FROM users').get();
    expect(v1.person_id).toBe(vp1.id);
    expect(v1.name).toBe('hello');

    const v2 = db.prepare('SELECT * FROM password_identifiers').all();
    const passwordHash = Auth.hashPassword('hello', 'Hello1234!', KohanaJS.config.auth.salt);
    expect(v2.length).toBe(1);
    const identifier = v2[0];
    expect(identifier.password).toBe(passwordHash);
    expect(identifier.identifier).toBe('email:test@example.com');
    expect(identifier.verified).toBe(0);

    expect(c.request.session.logged_in).toBe(true);
  });

  test('username already use', async () => {
    const c0 = new ControllerAuth({ headers: {}, body: 'name=test&email=test@example.com&username=hello&password=Hello1234!', cookies: {} });
    const r0 = await c0.execute('signup_post');

    const c = new ControllerAuth({ headers: {}, body: 'name=foo&email=test2@example.com&username=hello&password=Antoher1234!', cookies: {} });
    const r = await c.execute('signup_post');
    expect(r.status).toBe(500);
    expect(c.error.message).toBe('User Name hello already in use.');
  });

  test('email already use', async () => {
    const c0 = new ControllerAuth({ headers: {}, body: 'name=test&email=test@example.com&username=hello&password=Hello1234!', cookies: {} });
    const r0 = await c0.execute('signup_post');

    const c = new ControllerAuth({ headers: {}, body: 'name=foo&email=test@example.com&username=another&password=Antoher1234!', cookies: {} });
    const r = await c.execute('signup_post');
    expect(r.status).toBe(500);
    expect(c.error.message).toBe('Email test@example.com already in use. Please use another email');
  });

  test('auto assign first name and last name', async () => {
    const c = new ControllerAuth({ headers: {}, body: 'name=Peter%20Pan&email=test2@example.com&username=hello&password=Antoher1234!', cookies: {} });
    const r = await c.execute('signup_post');

    if (r.status === 500)console.log(c.error);
    const vp1 = db.prepare('SELECT * FROM persons').get();
    expect(vp1.first_name).toBe('Peter');
    expect(vp1.last_name).toBe('Pan');
  });
});
