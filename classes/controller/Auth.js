/* Controller auth handle login, logout */
const { Controller } = require('@kohanajs/core-mvc');
const { ORM, ControllerMixinDatabase, ControllerMixinMime, ControllerMixinView, KohanaJS } = require('kohanajs');
const { ControllerMixinMultipartForm } = require('@kohanajs/mod-form');
const { DatabaseDriverBetterSQLite3 } = require('@kohanajs/mod-database');
const { ControllerMixinSession } = require('@kohanajs/mod-session');

const Auth = require('../Auth');
const ControllerMixinAuth = require('../controller-mixin/Auth');
const ControllerMixinEmailRegister = require('../controller-mixin/EmailRegister');
const HelperRegistrar = require('../helper/Registrar');

const User = ORM.require('User');
const Person = ORM.require('Person');
const Role = ORM.require('Role');
const PasswordIdentifier = ORM.require('PasswordIdentifier');

class ControllerAuth extends Controller.mixin([
  ControllerMixinMultipartForm,
  ControllerMixinDatabase,
  ControllerMixinSession,
  ControllerMixinAuth,
  ControllerMixinEmailRegister,
  ControllerMixinMime,
  ControllerMixinView,
]) {
  constructor(request) {
    super(request);

    this.state.get(ControllerMixinDatabase.DATABASE_MAP)
      .set('session', `${KohanaJS.config.auth.databasePath}/session.sqlite`)
      .set('admin', `${KohanaJS.config.auth.databasePath}/admin.sqlite`);

    this.state.set(ControllerMixinDatabase.DATABASE_DRIVER, DatabaseDriverBetterSQLite3);
  }

  async action_signup_post() {
    const $_POST = this.state.get(ControllerMixinMultipartForm.POST_DATA);
    const { name, email, username, password, destination = KohanaJS.config.signup.destination } = $_POST;

    const role_id = (KohanaJS.config.signup.allowPostAssignRoleID && $_POST.role_id) ? $_POST.role_id : KohanaJS.config.signup.defaultRoleID;
    const adminDB = this.state.get(ControllerMixinDatabase.DATABASES).get('admin');

    const existUser = await ORM.readBy(User, 'name', [username], { database: adminDB });
    if (existUser) {
      throw new Error(`User Name ${username} already in use.`);
    }

    const res = await ORM.readBy(PasswordIdentifier, 'identifier', [`email:${email}`], { database: adminDB });
    if (res) {
      throw new Error(`Email ${email} already in use. Please use another email`);
    }

    let firstName;
    let lastName = $_POST.last_name || '';
    if (/\s/.test(name)) {
      const nameParts = name.split(' ');

      lastName = nameParts.pop();
      firstName = nameParts.join(' ');
    } else {
      firstName = $_POST.first_name || name;
    }

    const person = ORM.create(Person, { database: adminDB });
    person.first_name = firstName;
    person.last_name = lastName;
    person.email = email;
    await person.write();

    const user = ORM.create(User, { database: adminDB });
    user.person_id = person.id;
    user.name = username;
    await user.write();

    const role = await ORM.factory(Role, role_id, {database: adminDB});
    await user.add(role);
    user.roles = [role];

    const identifier = ORM.create(PasswordIdentifier, { database: adminDB });
    identifier.identifier = `email:${email}`;
    identifier.password = Auth.hashPassword(username, password, KohanaJS.config.auth.salt);
    identifier.user_id = user.id;
    identifier.verified = !KohanaJS.config.signup.requireActivate;
    await identifier.write();

    // generate activate code
    if (KohanaJS.config.signup.requireActivate) {
      const helperRegistrar = new HelperRegistrar(this.request.hostname, this.clientIP);
      await helperRegistrar.sendActivateCode(username, email);
    }

    const { session } = this.request;

    Object.assign(session, {
      user_full_name: `${person.first_name} ${person.last_name}`,
      user_role: role.name,
      user_id: user.id,
      role_ids: user.roles.map(it => it.id),
      roles: user.roles.map(it => it.name),
      logged_in: true,
    });

    await this.redirect(destination);
  }

  async action_login() {
    this.setTemplate('templates/login', {
      destination: this.request.query.cp || KohanaJS.config.signup.destination,
      message: '',
    });
  }

  async action_logout() {
    this.setTemplate('templates/login', {
      destination: KohanaJS.config.signup.destination,
      message: 'User Log Out Successfully.',
    });
  }

  async action_fail() {
    this.setTemplate('templates/login', {
      destination: this.request.query.cp,
      message: 'Login fail.',
    });
  }

  async action_auth() {
    const {
      destination = KohanaJS.config.signup.destination,
    } = this.state.get(ControllerMixinMultipartForm.POST_DATA);

    if (!this.state.get(ControllerMixinAuth.USER)) {
      await this.redirect(`/login/fail?cp=${encodeURIComponent(destination)}`);
      return;
    }
    await this.redirect(destination || KohanaJS.config.signup.destination);
  }

  async action_forgot_password() {
    this.setTemplate('templates/forgot-password/form');
  }

  async action_forgot_password_post() {
    await this.redirect('/forgot-password-submit');
  }

  async action_forgot_password_result() {
    this.setTemplate('templates/forgot-password/submit');
  }

  async action_reset_password() {
    const { sign } = this.request.params;
    const { code } = this.request.params;

    this.setTemplate('templates/reset-password/form', { sign, code });
  }

  async action_reset_password_post() {
    await this.redirect('/reset-password-submit');
  }

  async action_reset_password_result() {
    this.setTemplate('templates/reset-password/submit');
  }

  async action_forgot_username() {
    this.setTemplate('templates/forgot-username/form');
  }

  async action_forgot_username_post() {
    await this.redirect('/forgot-username-submit');
  }

  async action_forgot_username_result() {
    this.setTemplate('templates/forgot-username/submit');
  }
}

module.exports = ControllerAuth;
